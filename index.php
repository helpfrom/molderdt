<?php
// bootstrap.php
// Include Composer Autoload (relative to project root).
require_once "vendor/autoload.php";

use MolderDT\CreateMolderDT;

$date = new CreateMolderDT('pt_br');
$dateUs = new CreateMolderDT('us');
$dateFr = new CreateMolderDT('fr');

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Date molder</title>
    </head>
    <body>
        <?php
        echo $date->formatDateTime()."\n";
        echo $date->getWeekAbbr()."\n";
        ?>
        <?php echo $dateUs->formatDateTime()."\n"; ?>
        <?php echo $dateFr->formatDateTime()."\n"; ?>
    </body>
</html>