# README #

This examples use compose.

### What is this repository for? ###

* A easy way to implement a return format date for other countries.
* Version 1.0

### How do I get set up? ###

* Every CreateMolderDT item has to set properly.

### Contribution guidelines ###

* Test over PHP5.5.12 and PHP5.6.17

```
#!php

use MolderDT\CreateMolderDT;

$date = new CreateMolderDT('pt_br');
$dateUs = new CreateMolderDT('us');
$dateFr = new CreateMolderDT('fr');

echo $date->formatDateTime()."\n"; //2016-04-13 17:57:19
echo $date->getWeekAbbr()."\n"; //Qua

echo $dateUs->formatDateTime()."\n"; //2016-04-13 17:57:19
echo $dateFr->formatDateTime()."\n"; //2016-04-13 17:57:19

```


### Who do I talk to? ###

* Samuel Prates <samuelprates@yahoo.com.br>