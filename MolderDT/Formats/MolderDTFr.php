<?php

namespace MolderDT\Formats;

use DateTime;
use MolderDT\Formats\MolderDTInterface;

/**
 * Description of DatemolderFr
 *
 * @author Samuel Prates
 */
class MolderDTFr extends DateTime implements MolderDTInterface
{

    protected $outputDate       = 'd/m/Y';
    protected $outputDateTime   = 'd/m/Y H:i:s';

    /**
     * Retorna o nome do Mês completo
     * Get the complete Month's name
     * @return String String com nome do mês / String Month's name
     */
    public function getMonthName(){
        switch($this->format('n')){
            case 1:  return 'Janvier';  break;
            case 2:  return 'Février';break;
            case 3:  return 'Mars';    break;
            case 4:  return 'Avril';    break;
            case 5:  return 'Mai';     break;
            case 6:  return 'Juin';    break;
            case 7:  return 'Juillet';    break;
            case 8:  return 'Août';   break;
            case 9:  return 'Septembre'; break;
            case 10: return 'Octobre';  break;
            case 11: return 'Novembre'; break;
            case 12: return 'Décembre'; break;
        }
    }

    /**
     * Retorna o nome do Mês Abreviado
     * Get the abbreviated Month's name
     * @return String String com nome do mês / String abbreviated Month's name
     */
    public function getMonthAbbr(){
        switch($this->format('n')){
            case 1:  return 'Jan'; break;
            case 2:  return 'Fév'; break;
            case 3:  return 'Mar'; break;
            case 4:  return 'Avr'; break;
            case 5:  return 'Mai'; break;
            case 6:  return 'Jun'; break;
            case 7:  return 'Jul'; break;
            case 8:  return 'Aoû'; break;
            case 9:  return 'Sep'; break;
            case 10: return 'Oct'; break;
            case 11: return 'Nov'; break;
            case 12: return 'Déc'; break;
        }
    }

    /**
     * Retorna o nome do dia da semana completo
     * Get the Week's name
     * @return String String com nome do dia da semana / Get the Week's name
     */
    public function getWeekName(){
        switch($this->format('w')){
            case 0:  return 'Dimanche'; break;
            case 1:  return 'Lundi'; break;
            case 2:  return 'Mardi';   break;
            case 3:  return 'Mercredi';  break;
            case 4:  return 'Jeudi';  break;
            case 5:  return 'Vendredi';   break;
            case 6:  return 'Samedi';  break;
        }
    }

    /**
     * Retorna o nome do dia da semana abreviado
     * Get the abbreviated Week's name
     * @return String String com nome do dia da semana / Get the abbreviated Week's name
     */
    public function getWeekAbbr(){
        switch($this->format('w')){
            case 0:  return 'Dim'; break;
            case 1:  return 'Lun'; break;
            case 2:  return 'Mar'; break;
            case 3:  return 'Mer'; break;
            case 4:  return 'Jeu'; break;
            case 5:  return 'Ven'; break;
            case 6:  return 'Sam'; break;
        }
    }

}
