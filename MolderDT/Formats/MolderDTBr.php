<?php

namespace MolderDT\Formats;

use DateTime;
use MolderDT\Formats\MolderDTInterface;
/**
 * This class was made to extends Datemolder object
 * to better use of Brasilian developers
 */
class MolderDTBr extends DateTime implements MolderDTInterface{

    protected $outputDate       = 'd/m/Y';
    protected $outputDateTime   = 'd/m/Y H:i:s';

    /** 
     * Retorna o nome do Mês completo
     * Get the complete Month's name
     * @return String String com nome do mês / String Month's name
     */
    public function getMonthName(){
        switch($this->format('n')){
            case 1:  return 'Janeiro';  break;
            case 2:  return 'Fevereiro';break;
            case 3:  return 'Março';    break;
            case 4:  return 'Abril';    break;
            case 5:  return 'Maio';     break;
            case 6:  return 'Junho';    break;
            case 7:  return 'Julho';    break;
            case 8:  return 'Agosto';   break;
            case 9:  return 'Setembro'; break;
            case 10: return 'Outubro';  break;
            case 11: return 'Novembro'; break;
            case 12: return 'Dezembro'; break;
        }
    }

    /**
     * Retorna o nome do Mês Abreviado
     * Get the abbreviated Month's name
     * @return String String com nome do mês / String abbreviated Month's name
     */
    public function getMonthAbbr(){
        switch($this->format('n')){
            case 1:  return 'Jan'; break;
            case 2:  return 'Fev'; break;
            case 3:  return 'Mar'; break;
            case 4:  return 'Abr'; break;
            case 5:  return 'Mai'; break;
            case 6:  return 'Jun'; break;
            case 7:  return 'Jul'; break;
            case 8:  return 'Ago'; break;
            case 9:  return 'Set'; break;
            case 10: return 'Out'; break;
            case 11: return 'Nov'; break;
            case 12: return 'Dez'; break;
        }
    }

    /**
     * Retorna o nome do dia da semana completo
     * Get the Week's name
     * @return String String com nome do dia da semana / Get the Week's name
     */
    public function getWeekName(){
        switch($this->format('w')){
            case 0:  return 'Domingo'; break;
            case 1:  return 'Segunda'; break;
            case 2:  return 'Terça';   break;
            case 3:  return 'Quarta';  break;
            case 4:  return 'Quinta';  break;
            case 5:  return 'Sexta';   break;
            case 6:  return 'Sábado';  break;
        }
    }

    /**
     * Retorna o nome do dia da semana abreviado
     * Get the abbreviated Week's name
     * @return String String com nome do dia da semana / Get the abbreviated Week's name
     */
    public function getWeekAbbr(){
        switch($this->format('w')){
            case 0:  return 'Dom'; break;
            case 1:  return 'Seg'; break;
            case 2:  return 'Ter'; break;
            case 3:  return 'Qua'; break;
            case 4:  return 'Qui'; break;
            case 5:  return 'Sex'; break;
            case 6:  return 'Sáb'; break;
        }
    }

}