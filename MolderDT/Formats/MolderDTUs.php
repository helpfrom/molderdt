<?php

namespace MolderDT\Formats;

use DateTime;
use MolderDT\Formats\MolderDTInterface;
/**
 * Description of DatemolderUs
 *
 * @author Samuel Prates
 */
class MolderDTUs extends DateTime implements MolderDTInterface
{

    protected $outputDate       = 'm/d/Y';
    protected $outputDateTime   = 'm/d/Y H:i:s';

    /**
     * Retorna o nome do Mês completo
     * Get the complete Month's name
     * @return String String com nome do mês / String Month's name
     */
    public function getMonthName(){
        $this->format('F');
    }

    /**
     * Retorna o nome do Mês Abreviado
     * Get the abbreviated Month's name
     * @return String String com nome do mês / String abbreviated Month's name
     */
    public function getMonthAbbr(){
        return $this->format('M');
    }

    /**
     * Retorna o nome do dia da semana completo
     * Get the Week's name
     * @return String String com nome do dia da semana / Get the Week's name
     */
    public function getWeekName(){
        return $this->format('l');
    }

    /**
     * Retorna o nome do dia da semana abreviado
     * Get the abbreviated Week's name
     * @return String String com nome do dia da semana / Get the abbreviated Week's name
     */
    public function getWeekAbbr(){
        return $this->format('D');
    }

    
}
