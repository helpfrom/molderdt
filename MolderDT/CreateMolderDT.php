<?php
namespace MolderDT;

use DateTime;
use Exception;
use MolderDT\Formats\MolderDTInterface;

/**
 * This class was made to extends PHP DateTime object
 * to better use of International developers
 */
class CreateMolderDT extends DateTime{

    protected   $outputDate       = 'Y-m-d';
    protected   $outputDateTime   = 'Y-m-d H:i:s';
    /**
     *
     * @var  MolderDTInterface
     */
    private     $formated;
    private     $base;
    private     $molders          = [
        'pt_br' => 'MolderDT\\Formats\\MolderDTBr',
        'fr' => 'MolderDT\\Formats\\MolderDTFr',
        'us' => 'MolderDT\\Formats\\MolderDTUs'
    ];

    public function __construct($base, $time = NULL, $object = NULL)
    {
        if( is_null($base) || $base == '' || !isset($this->molders['pt_br']) ){
            throw new Exception('This format does not exist.');
        } else {
            $this->base     = $base;
            $this->formated = new $this->molders[$base]($time, $object);
        }
        parent::__construct($time, $object);
    }

    /**
    * Get the format of date (without Hour) for usage in inserts
    * @return String String of formated date
    */
    public function getFormatDateDB(){
        return $this->format('Y-m-d');
    }

    /**
    * Get the format of date (with Hour) for usage in inserts
    * @return String String of formated date
    */
    public function getFormatDateTimeDB(){
        return $this->format('Y-m-d H:i:s');
    }

    /**
    * Get a new instance of this object from a format given
    * @return Object Instance Datemolder objeto
    */
    public static function fromFormat($base, $format, $time){
        if( is_null($base) || $base == '' || !isset($this->molders['pt_br']) ){
            throw new Exception('This format does not exist.');
        }
        return new self(parent::createFromFormat($format, $time)->format('Y-m-d H:i:s'));
    }

    /**
    * Get the format of date (without Hour) set by $this->outputDate
    * @return String String of formated date
    */
    public function formatDate(){
        return $this->format($this->outputDate);
    }

    /**
    * Get the format of date (with Hour) set by $this->outputDate
    * @return String String of formated date
    */
    public function formatDateTime(){
        return $this->format($this->outputDateTime);
    }

    public function __toString() {
        return $this->formatDate();
    }

    /**
     * Retorna o nome do Mês completo
     * Get the complete Month's name
     * @return String String com nome do mês / String Month's name
     */
    public function getMonthName(){
        $this->formated->getMonthName();
    }

    /**
     * Retorna o nome do Mês Abreviado
     * Get the abbreviated Month's name
     * @return String String com nome do mês / String abbreviated Month's name
     */
    public function getMonthAbbr(){
        return $this->formated->getMonthAbbr();
    }

    /**
     * Retorna o nome do dia da semana completo
     * Get the Week's name
     * @return String String com nome do dia da semana / Get the Week's name
     */
    public function getWeekName(){
        return $this->formated->getWeekName();
    }

    /**
     * Retorna o nome do dia da semana abreviado
     * Get the abbreviated Week's name
     * @return String String com nome do dia da semana / Get the abbreviated Week's name
     */
    public function getWeekAbbr(){
        return $this->formated->getWeekAbbr();
    }

}